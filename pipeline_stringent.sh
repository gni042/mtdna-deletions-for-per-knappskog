#!/bin/bash

#### FILES #####
valid_ids="/data1/per_mtdna/ids"
reference_genome="/ref/genomes/b37_decoy/b37_decoy.fasta"
illumina_adapters="/opt/Trimmomatic-0.36/adapters/TruSeq3-SE.fa"
region_of_interest="/data1/per_mtdna/region_of_interest.bed"

#### VARIABLES ####
mtchr="MT"
insert_size=340

#### FOLDERS TO GENERATE ####
mkdir -p /data1/per_mtdna/results/trimmed_fq
#mkdir -p /data1/per_mtdna/results/fastqfiles_no-adaptors
mkdir -p /data1/per_mtdna/results/logs
mkdir -p /data1/per_mtdna/results/bamfiles
mkdir -p /data1/per_mtdna/results/insertsizes
mkdir -p /data1/per_mtdna/results/contigs
mkdir -p /data1/per_mtdna/results/vcffiles
mkdir -p /data1/per_mtdna/results/qc
mkdir -p /data1/per_mtdna/results/pindel_configs_strict
mkdir -p /data1/per_mtdna/results/pindel_output_strict
#mkdir -p /data1/per_mtdna/results/depth
#mkdir -p /data1/per_mtdna/results/permutations
#mkdir -p /data1/per_mtdna/results/tmp
mkdir -p /data1/per_mtdna/tmp

#### PROGRAMS ####
trimmomatic_jar="/opt/Trimmomatic-0.39/trimmomatic-0.39.jar"
samtools_exe="/opt/samtools-1.9/samtools"
bwa_exe="/usr/local/bin/bwa-0.7.17"
gatk_jar="/opt/GenomeAnalysisTK-3.8.1/GenomeAnalysisTK.jar"
picard_jar="/opt/PicardTools-2.21.1/picard.jar"
# PINDEL THAT WORKS: https://github.com/genome/pindel/archive/70c1bb4a75503da39e206e02178fe3d8a0afdf81.tar.gz
pindel_exe="/opt/pindel-0.2.5b9/pindel"
bedtools_exe="/opt/bedtools-2.29.0/bin/bedtools"
bam_readcount_exe="/opt/bam-readcount-0.8.0/bam-readcount"
# https://github.com/ding-lab/Pindel2BAM
#pindel2sam_exe="/opt/Pindel2BAM/pin2sam"
# This version here does not remove the /1 or /2 of the read ids:
pindel2sam_exe="/data1/per_mtdna/scripts/pin2sam"
get_deletion_flanks_script="/data1/per_mtdna/scripts/get_deletion_flanks.py"
summarise_deletions_script="/data1/per_mtdna/scripts/summarise_deletions.py"
remove_small_deletions_script="/data1/per_mtdna/scripts/remove_small_deletions.py"


###############################################################################
#   TRIMMING                                                                  #
###############################################################################

echo "( 1) TRIMMING"

for id in `cat ${valid_ids}`; do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz"
    out2="/data1/per_mtdna/results/trimmed_fq/${id}_unpaired.R1.fastq.gz"
    out3="/data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz"
    out4="/data1/per_mtdna/results/trimmed_fq/${id}_unpaired.R2.fastq.gz"
    if [ -s "${out1}" ] && [ -s "${out2}" ] && [ -s "${out3}" ] && [ -s "${out4}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "     Trimming!"
        java -jar ${trimmomatic_jar} PE \
            /data1/per_mtdna/fastqfiles/${id}.R1.fastq.gz \
            /data1/per_mtdna/fastqfiles/${id}.R2.fastq.gz \
        	${out1} \
	    	${out2} \
            ${out3} \
            ${out4} \
	    	ILLUMINACLIP:${illumina_adapters}:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 \
            2> /data1/per_mtdna/results/logs/${id}_trimming.stderr
    fi
done

###############################################################################
#   ALIGNING WITH BWA                                                         #
###############################################################################

echo "( 2) ALIGNING WITH BWA"

for id in `cat ${valid_ids}`; do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/bamfiles/${id}.bam"
    out2="/data1/per_mtdna/results/bamfiles/${id}.bam.bai"
    if [ -s "${out1}" ] && [ -s "${out2}" ]
    then
        echo "     Output files already present, skipping"
    else
        ${bwa_exe} mem -t 16 \
            -R "@RG\tID:foo\tSM:bar" \
            ${reference_genome} \
            /data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz \
            /data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz \
        2> /data1/per_mtdna/results/logs/bwa1_${id}.stderr | \
        ${samtools_exe} view -b -h -@ 16 - | \
        ${samtools_exe} sort -@ 16 -o ${out1} -
        ${samtools_exe} index ${out1}
    fi
done

###############################################################################
#   CALCULATING mtDNA EFFICIENCY AND INSERT SIZES                             #
###############################################################################

echo "MAPPING EFFICIENCY..."

for id in `cat ${valid_ids}`
do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/insertsizes/${id}.txt"
    out2="/data1/per_mtdna/results/insertsizes/${id}.pdf"
    if [ -s "${out1}" ] && [ -s "${out2}" ]
    then
        echo "     Output files already present, skipping"
    else
        java -jar /opt/picard-2.6.0/picard.jar CollectInsertSizeMetrics \
            I=/data1/per_mtdna/results/bamfiles/${id}.bam \
            O=${out1} \
            H=${out2} \
            2> /data1/per_mtdna/results/logs/mapping_eff.stderr
    fi
done

out1="/data1/per_mtdna/results/mt_vs_total_reads.tsv"
if [ ! -s "${out1}" ]
then
    for id in `cat ${valid_ids}`
    do
        ALL=$( ${samtools_exe} view -F 4 -q 30 -c /data1/per_mtdna/results/bamfiles/${id}.bam )
        MT=$( ${samtools_exe} view -F 4 -q 30 -c /data1/per_mtdna/results/bamfiles/${id}.bam ${mtchr} )
        echo -e "${id}\t${MT}\t${ALL}"
    done > ${out1}
fi


###############################################################################
#   CREATING NEW CONTIG TO REALIGN                                            #
###############################################################################

echo "( 4) CREATING NEW CONTIG AND REALIGNING"
 
for id in `cat ${valid_ids}`; do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/bamfiles/${id}_dedup.bam"
    out2="/data1/per_mtdna/results/qc/${id}_dup_metrics.txt"
    out3="/data1/per_mtdna/results/bamfiles/${id}_dedup.bam.bai"
    out4="/data1/per_mtdna/results/vcffiles/${id}.vcf"
    out5="/data1/per_mtdna/results/vcffiles/${id}_filtered.vcf"
    out6="/data1/per_mtdna/results/contigs/${id}.fa"
    realigned_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam"
    out8="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam.bai"
    nonclipped_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam"
    if [ -s "${out5}" ] && [ -s "${realigned_bam}" ] && [ -s "${out8}" ] && [ -s "${nonclipped_bam}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "      (a) Marking duplicates"
        java -jar ${picard_jar} MarkDuplicates \
            I=/data1/per_mtdna/results/bamfiles/${id}.bam \
            O=${out1} \
            M=${out2} \
            2> /data1/per_mtdna/results/logs/MarkDuplicates_${id}.stderr
        ${samtools_exe} index /data1/per_mtdna/results/bamfiles/${id}_dedup.bam

        echo "      (b) Finding SNPs in ${mtchr} chromosome"
        java -jar ${gatk_jar} \
            -R ${reference_genome} \
            -T HaplotypeCaller \
            -ploidy 1 \
            -L ${mtchr} \
            -I ${out1} \
            -o ${out4} \
        2> /data1/per_mtdna/results/logs/HaplotypeCaller_${id}.stderr > /data1/per_mtdna/results/logs/HaplotypeCaller_${id}.stdout

        echo "      (c) Filtering SNPs"
        java -jar ${gatk_jar} \
            -T SelectVariants \
            -R ${reference_genome} \
            -V ${out4} \
            -L ${mtchr} \
            -selectType SNP \
            -select "QD > 10.0 || FS < 20.0 || QUAL > 1000.0" \
            -o ${out5} \
        2> /data1/per_mtdna/results/logs/SelectVariants_${id}.stderr > /data1/per_mtdna/results/logs/SelectVariants_${id}.stdout

        echo "      (d) Creating alternate reference with selected SNPs"
        java -jar ${gatk_jar} \
            -R ${reference_genome} \
            -T FastaAlternateReferenceMaker \
            -o ${out6} \
            -V ${out5} \
        2> /data1/per_mtdna/results/logs/FastaAlternateReferenceMaker_${id}.stdout > /data1/per_mtdna/results/logs/FastaAlternateReferenceMaker_${id}.stderr

        sed "s/^>.*${mtchr}:1$/>${mtchr}/" ${out6} > ${out6}.tmp
        mv ${out6}.tmp ${out6}

        echo "      (e) Indexing alternate reference"
        ${bwa_exe} index ${out6} 2> /data1/per_mtdna/results/logs/bwaIndex_${id}.stderr > /data1/per_mtdna/results/logs/bwaIndex_${id}.stdout

        echo "      (f) Re-aligning to alternate reference"
        ${bwa_exe} mem -t 16 \
            -R "@RG\tID:foo\tSM:bar" \
            ${out6} \
            /data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz \
            /data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz \
        2> /data1/per_mtdna/results/logs/bwa2_${id}.stderr | \
        ${samtools_exe} view -b -h -F 2048 -@ 16 - | \
        ${samtools_exe} sort -@ 16 -o ${realigned_bam} -
        ${samtools_exe} index ${realigned_bam}

        echo "      (g) Removing soft- and hard-clipped reads from alignment"
        ${samtools_exe} view -q 30 -h -@ 16 ${realigned_bam} ${mtchr} | \
            awk -F '\t' '($6 !~ /H|S/)' | \
            ${samtools_exe} view -b -h -@ 16 - \
            > ${nonclipped_bam}
        echo "      (h) Indexing cleaned up alignement"
        ${samtools_exe} index ${nonclipped_bam}
    fi
done

## To merge vcfs into a single multisample vcf,
## bash /data1/per_mtdna/scripts/merge_vcfs.sh
## bash /data1/per_mtdna/scripts/merge_vcfs_filtered.sh
## ... and by hand removed variants outside 5800-14860
#
###############################################################################
#   FINDING DELS WITH PINDEL                                                  #
###############################################################################

mdl=100 # Minimum deletion length

echo "( 5) FINDING DELETIONS WITH PINDEL"

for id in `cat ${valid_ids}`
do
    echo "     ${id}"
    cfg_file="/data1/per_mtdna/results/pindel_configs_strict/${id}.cfg"
    contig_ref="/data1/per_mtdna/results/contigs/${id}.fa"
    fai_index="/data1/per_mtdna/results/contigs/${id}.fa.fai"
    pindel_file="/data1/per_mtdna/results/pindel_output_strict/${id}_D"
    splitreads_bam="/data1/per_mtdna/results/bamfiles/${id}_splitreads_strict.bam"
    if [ -s "${cfg_file}" ] && [ -s "${fai_index}" ] && [ -s "${pindel_file}" ] && [ -s "${splitreads_bam}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "      (a) Running PINDEL"
        echo -e "/data1/per_mtdna/results/bamfiles/${id}_realigned.bam\t${insert_size}\t${id}" \
            > ${cfg_file}
        ${samtools_exe} faidx ${contig_ref}
        ${pindel_exe} -f ${contig_ref} \
            -i ${cfg_file} \
            -c ${mtchr}:1,000-15,000 \
            -T 16 \
            -E 0.75 \
            -a 5 \
            -m 6 \
            -e 0 \
            -x 5 \
            -r false \
            -t false \
            -l false \
            -k true \
            -j ${region_of_interest} \
            -A 30 \
            -L /data1/per_mtdna/results/logs/pindel_strict_${id}.log \
            -o /data1/per_mtdna/results/pindel_output_strict/${id} \
        > /data1/per_mtdna/results/logs/pindel_strict_${id}.stdout 2> /data1/per_mtdna/results/logs/pindel_strict_${id}.stderr
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_BP
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_CloseEndMapped
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_INT_final
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_LI
        echo "      (b) Creating alignment of split reads"
        rm -rf /data1/per_mtdna/tmp/*
        python3.6 ${remove_small_deletions_script} ${pindel_file} ${mdl} > /data1/per_mtdna/tmp/${id}_D
        ${pindel2sam_exe} tmp \
            /data1/per_mtdna/tmp \
            ${cfg_file} \
            ${fai_index} \
            2> /data1/per_mtdna/results/logs/pindel2sam_strict_${id}.stderr \
            > /data1/per_mtdna/results/logs/pindel2sam_strict_${id}.stdout
        OUTFILE=/data1/per_mtdna/tmp/$(cat ${cfg_file} | cut -f1 | sed "s/\//_/g").sam
        ${samtools_exe} view -b -h -@ 16 ${OUTFILE} | \
            ${samtools_exe} sort -@ 16 -o ${splitreads_bam} \
            2> /dev/null
        ${samtools_exe} index ${splitreads_bam}
        rm ${OUTFILE} /data1/per_mtdna/tmp/${id}_D
    fi
done
###############################################################################
#   PARSING PINDEL OUTPUT                                                     #
###############################################################################

mr=1   # Minimum number of reads supporting a deletion
mdl=100 # Minimum deletion length
echo "( 6) PARSING PINDEL OUTPUT (min_reads=${mr}, min_del_length=${mdl})"

for id in `cat ${valid_ids}`
do
    echo "     ${id}"
    pindel_file="/data1/per_mtdna/results/pindel_output_strict/${id}_D"
    bam="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam"
    name_sorted_tmp="/data1/per_mtdna/tmp/${id}_realigned_namesorted.bam"
    contig_ref="/data1/per_mtdna/results/contigs/${id}.fa"
    fai_index="/data1/per_mtdna/results/contigs/${id}.fa.fai"
    cfg_file="/data1/per_mtdna/results/pindel_configs_strict/${id}.cfg"
    out_dels="/data1/per_mtdna/results/pindel_output_strict/${id}_D_dels_${mr}_${mdl}.dat"
    out_flanks="/data1/per_mtdna/results/pindel_output_strict/${id}_D_flanking_regions_${mr}_${mdl}.bed"
    out_flanks_sorted="/data1/per_mtdna/results/pindel_output_strict/${id}_D_flanking_regions_${mr}_${mdl}_sorted.bed"
    realigned_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam"
    nonclipped_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam"
    nondeleted_flanks="/data1/per_mtdna/results/pindel_output_strict/${id}_non-deleted_flanks_${mr}_${mdl}.csv"
    if [ -f "${out_dels}" ] && [ -f "${out_flanks}" ] && [ -f "${nondeleted_flanks}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "      (a) Filtering and tidying up Pindel breakpoints"
        python3.6 ${compute_breakpoints_script} ${pindel_file} ${mr} ${mdl} \
            2> /dev/null
        echo "      (b) Getting deletion flanking regions"
        ${samtools_exe} sort -n -@ 16 -o ${name_sorted_tmp} ${bam} 2> /dev/null
        python3.6 ${get_deletion_flanks_script} ${pindel_file} ${name_sorted_tmp} ${mr} ${mdl} ${mtchr}\
            2> /dev/null
        rm ${name_sorted_tmp}
        sort -n -k 2 ${out_flanks} \
            > ${out_flanks_sorted}
        echo "      (c) Merging interesting areas"
        ${bedtools_exe} merge -i ${out_flanks_sorted} \
            > ${out_flanks}
        rm ${out_flanks_sorted}
        echo "      (d) Bam read counting"
        ${bam_readcount_exe} -q 30 -b 30 -d 5000000 -w 1 \
            -l ${out_flanks} \
            -f ${contig_ref} ${nonclipped_bam} \
            2> /dev/null | cut -f2,6,7,8,9 \
            > ${nondeleted_flanks}
    fi
done

exit

# Generate a summary file with deletions for all samples

echo "   * Generating a summary of all deletion for all samples"
python3.6 ${summarise_deletions_script} /data1/per_mtdna/results/pindel_output_strict/*_D_dels_${mr}_${mdl}.dat \
    > /data1/per_mtdna/results/summary_deletions_${mr}_${mdl}.tsv

exit

