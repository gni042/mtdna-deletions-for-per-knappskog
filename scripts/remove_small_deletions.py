#!/usr/bin/python3

import os
import sys
import numpy
#import re
#import csv

if (len(sys.argv)!=3):
    print("USAGE: {program} {pindelfile} {min_del_length}".format(program=sys.argv[0],
          pindelfile="<pindel_outputfile_D>", min_del_length="MIN_DELETION_LENGTH"),
          file=sys.stderr)
    sys.exit(1)

deletions_file = sys.argv[1]

if not os.path.isfile(deletions_file):
    print("File \"{file}\" does not exist. Abort.".format(file=deletions_file), file=sys.stderr)
    sys.exit(1)


###############################################################################
#   PARAMETERS                                                                #
###############################################################################

# Number of upstream-anchored and downstream anchored supporting reads required
min_del_length = int(sys.argv[2])



###############################################################################
#   FUNCTIONS                                                                 #
###############################################################################

def get_groups(seq, group_by):
    data = []
    for line in seq:
        if line.startswith(group_by):
            if data:
                yield data
                data = []
        data.append(line)

    if data:
        yield data

###############################################################################
#   BODY                                                                      #
###############################################################################

with open( deletions_file, "r" ) as f:
    for deletion in get_groups(f, "##########"):
        if int(deletion[1].split()[2]) >= min_del_length:
            sys.stdout.write(''.join(deletion))

