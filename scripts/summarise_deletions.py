#!/usr/bin/python3

import os
import sys
#import numpy
import re
#import csv

if (len(sys.argv)<1):
    print("USAGE: {program} DEL_FILE1 [DEL_FILE2, ...]".format(program=sys.argv[0]),
          file=sys.stderr)
    sys.exit(1)

deletion_files = sys.argv[1:]

###############################################################################
#   FUNCTIONS                                                                 #
###############################################################################

def read_file( del_file ):
    dels = list()
    with open( del_file, "r" ) as f:
        for line in f:
            dels.append([int(x) for x in re.split('[ -]', line.strip())])
    return(dels)

# Also if they are CONSECUTIVE
def overlap_len( interval1, interval2 ):
    a = range(interval1[0], interval1[1]+1)
    b = range(interval2[0], interval2[1]+1)
    fst, snd = (a, b) if len(a) < len(b) else (b, a)
    if max(a) == (min(b)-1):
        return(-1)
    if max(b) == (min(a)-1):
        return(-1)
    return ( len(set(fst).intersection(snd)) )

def find_a_pair(Dels):
    for del1 in Dels:
        for del2 in Dels:
            if del1 == del2:
                continue
            match = 0
            del1_bp1_start = Dels[del1][0]
            del1_bp1_end   = Dels[del1][1]
            del1_bp2_start = Dels[del1][2]
            del1_bp2_end   = Dels[del1][3]
            del2_bp1_start = Dels[del2][0]
            del2_bp1_end   = Dels[del2][1]
            del2_bp2_start = Dels[del2][2]
            del2_bp2_end   = Dels[del2][3]
            # BP1
            if overlap_len([del1_bp1_start, del1_bp1_end], [del2_bp1_start, del2_bp1_end]) != 0:
                match += 1
            # BP2
            if overlap_len([del1_bp2_start, del1_bp2_end], [del2_bp2_start, del2_bp2_end]) != 0:
                match += 1
            if match == 2:
                return([del1, del2])
    return(["NONE","NONE"])
###############################################################################
#   BODY                                                                      #
###############################################################################

deletions = dict()

for f_name in deletion_files:
    if not os.path.isfile(f_name):
        print("File \"{file}\" does not exist. Abort.".format(file=f_name), file=sys.stderr)
        sys.exit(1)
    else:
        deletions[f_name] = read_file(f_name)

# Summarise deletions 
# Dels is a dictionary of DELETIONS, gathered in the first round loop
Dels = dict()
del_num = 1
for sample in deletions:
    #print("Looking at sample " + sample)
    for deletion in deletions[sample]:
        new_bp1_start = deletion[0]
        new_bp1_end = deletion[1]
        new_bp2_start = deletion[2]
        new_bp2_end = deletion[3]
        new_supports = deletion[4]
        #print(" - Deletion " + str(new_bp1_start) + "-" + 
        #        str(new_bp1_end) + " " + str(new_bp2_start) + "-" + 
        #        str(new_bp2_end) + " (" + str(new_supports) + ")")
        # Check that deletion is in the dels dict
        found = 0
        for Del_id in Dels:
            present = 0
            bp1_start = Dels[Del_id][0]
            bp1_end   = Dels[Del_id][1]
            bp2_start = Dels[Del_id][2]
            bp2_end   = Dels[Del_id][3]
            #print("   - Checking VS: " + Del_id + ":" + str(bp1_start) + "-" + 
            #        str(bp1_end) + " " + str(bp2_start) + "-" + str(bp2_end))
            if overlap_len([new_bp1_start, new_bp1_end], [bp1_start, bp1_end]) != 0:
                present += 1
                #print("     * YES, there is overlap in the STARTING BREAKPOINT")
            #else:
            #    print("     * NO OVERLAP in the START")
            if overlap_len([new_bp2_start, new_bp2_end], [bp2_start, bp2_end]) != 0:
                present += 1
                #print("     * YES, there is overlap in the ENDING BREAKPOINT")
            #else:
            #    print("     * NO OVERLAP in the END")
            if present == 2:
                #print("   ====> SAME DELETION (extending limits)!!")
                Dels[Del_id][0] = min(new_bp1_start, bp1_start)
                Dels[Del_id][1] = max(new_bp1_end, bp1_end)
                Dels[Del_id][2] = min(new_bp2_start, bp2_start)
                Dels[Del_id][3] = max(new_bp2_end, bp2_end)
                #print("         (and exiting loop)")
                found = 1
            #else:
            #    print("   ====> There was no overlap between the deletions")

        if found == 0:
            new_del_id = "Del_" + str(del_num)
            #print("     FINISHED CHECKING. No overlap found, adding " + new_del_id + " to the list")
            # Add new deletion to Dels
            Dels[new_del_id] = list()
            Dels[new_del_id].append(new_bp1_start)
            Dels[new_del_id].append(new_bp1_end)
            Dels[new_del_id].append(new_bp2_start)
            Dels[new_del_id].append(new_bp2_end)
            del_num += 1


still_pairs = 1
while still_pairs == 1:
    Pairs = find_a_pair(Dels)
    if Pairs[0] == "NONE":
        still_pairs = 0
    else:
        Dels[Pairs[0]][0] = min(Dels[Pairs[0]][0], Dels[Pairs[1]][0])
        Dels[Pairs[0]][1] = max(Dels[Pairs[0]][1], Dels[Pairs[1]][1])
        Dels[Pairs[0]][2] = min(Dels[Pairs[0]][2], Dels[Pairs[1]][2])
        Dels[Pairs[0]][3] = max(Dels[Pairs[0]][3], Dels[Pairs[1]][3])
        Dels.pop(Pairs[1])


# Second round, where each sample is checked for each deletion and supports added

supports = dict()
for Del_id in Dels:
    supports[Del_id] = list()

for Del_id in Dels:
    bp1_start = Dels[Del_id][0]
    bp1_end   = Dels[Del_id][1]
    bp2_start = Dels[Del_id][2]
    bp2_end   = Dels[Del_id][3]
    #print("Looking at deletion " + Del_id + ": " + str(bp1_start) + "-" + str(bp1_end) + " " + str(bp2_start) + "-" + str(bp2_end))

    for sample in deletions:
        added = 0
        #print(" - Sample " + sample)
        for deletion in deletions[sample]:
            present = 0
            new_bp1_start = deletion[0]
            new_bp1_end = deletion[1]
            new_bp2_start = deletion[2]
            new_bp2_end = deletion[3]
            new_supports = deletion[4]
            #print("   - Sample del: " + str(new_bp1_start) + "-" + 
            #        str(new_bp1_end) + " " + str(new_bp2_start) + "-" + str(new_bp2_end))
            if overlap_len([new_bp1_start, new_bp1_end], [bp1_start, bp1_end]) > 0:
                #print("     * YES, there is overlap in the STARTING BREAKPOINT")
                present += 1
            #else:
            #    print("     * NO OVERLAP in the START")
            if overlap_len([new_bp2_start, new_bp2_end], [bp2_start, bp2_end]) > 0:
                #print("     * YES, there is overlap in the ENDING BREAKPOINT")
                present += 1
            #else:
            #    print("     * NO OVERLAP in the END")
            if present == 2:
                #print("   ====> SAME DELETION (adding supports)")
                if added == 0:
                    supports[Del_id].append(new_supports)
                    added = 1
                else:
                    supports[Del_id][-1] += new_supports
            else:
                # Reset presence counter for next check
                #print("   ====> There was no overlap between the deletions")
                present = 0
        if added == 0:
            #print("     FINISHED CHECKING. No overlap found, adding 0 supports")
            supports[Del_id].append(0)

#print("DELS:")
#print(Dels)
#print("SUPPORTS:")
#print(supports)
#sys.exit()

# Print results
# Sample names (col names)
sys.stdout.write("del_id\tstart1\tstart2\tend1\tend2\t" +
        "\t".join(list(deletions.keys())) + "\n")
# Deletion supports
for del_id in Dels:
    sys.stdout.write(del_id + "\t" + 
            "\t".join([str(x) for x in Dels[del_id]]) + "\t" +
            "\t".join([str(x) for x in supports[del_id]]) + "\n")




