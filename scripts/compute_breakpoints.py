#!/usr/bin/python3

import os
import sys
import numpy
#import re
#import csv

if (len(sys.argv)!=4):
    print("USAGE: {program} {pindelfile} {min_reads} {min_del_length}".format(program=sys.argv[0],
          pindelfile="<pindel_outputfile_D>", min_reads="MIN_SUPPORTING_READS", min_del_length="MIN_DELETION_LENGTH"),
          file=sys.stderr)
    sys.exit(1)

deletions_file = sys.argv[1]

if not os.path.isfile(deletions_file):
    print("File \"{file}\" does not exist. Abort.".format(file=deletions_file), file=sys.stderr)
    sys.exit(1)


###############################################################################
#   PARAMETERS                                                                #
###############################################################################

# Number of upstream-anchored and downstream anchored supporting reads required
min_reads = int(sys.argv[2])
# Minimum deletion size
min_del_size = int(sys.argv[3])


###############################################################################
#   OUTPUT FILES                                                              #
###############################################################################

# Histogram of deletion lengths
length_hist_file = deletions_file + "_DelLen_" + str(min_reads) + "_" + str(min_del_size) + ".hist"
# Manhattan plot of starting point breaks (union)
start_hist_file = deletions_file + "_BPs1_" + str(min_reads) + "_" + str(min_del_size) + ".hist"
# Manhattan plot of ending point breaks (union)
end_hist_file = deletions_file + "_BPs2_" + str(min_reads) + "_" + str(min_del_size) + ".hist"
# Actual deletions (intersection)
BP_file = deletions_file + "_dels_" + str(min_reads) + "_" + str(min_del_size) + ".dat"

print("Output will be written in:\n{f1}\n{f2}\n{f3}\n{f4}".format(
            f1=length_hist_file,
            f2=start_hist_file,
            f3=end_hist_file,
            f4=BP_file),
            file=sys.stderr)
print("-----------", file=sys.stderr)


###############################################################################
#   FUNCTIONS                                                                 #
###############################################################################

def group_by_heading( source_file ):
    line_nb = 0
    for line in source_file:
        if line.startswith( "##########" ):
            line_nb = 1
        else:
            line_nb += 1
            if line_nb == 2:
                yield line.strip()


###############################################################################
#   BODY                                                                      #
###############################################################################

deletions = list()

a = 0
with open( deletions_file, "r" ) as f:
    for subheader in group_by_heading( f ):
        sh = subheader.split()
        if sh[1] != "D":
            print("Non-deletion event found: {sv}\nAborting.".format(sv=subheader[1]), file=sys.stderr)
            sys.exit(1)
        start_point = int(sh[9])
        end_point   = int(sh[10])
        start_R     = int(sh[12])
        end_R       = int(sh[13])
        
        start_diff = 0
        end_diff   = 0
        if start_point < start_R:
            print("Start and Range Start are incompatible: {s} {rs} ID={del_id}".format(s=start_point, rs=start_R, del_id=sh[0]))
        else:
            start_diff = start_point - start_R
        if end_point > end_R:
            print("End and Range End are incompatible: {s} {rs} ID={del_id}".format(s=end_point, rs=end_R, del_id=sh[0]))
        else:
            end_diff = end_R - end_point
        range_start = [ start_R, start_point + end_diff ]
        range_end   = [ end_point - start_diff, end_R ]
        deletions.append([ int(sh[0]), int(sh[2]), range_start, range_end, int(sh[18]), int(sh[19]), int(sh[21]), int(sh[22]), int(sh[2]) ])
        # [0] ID
        # [1] Pos
        # [2] Range start
        # [3] Range end
        # [4] Reads w/ upstream anchor
        # [5] " unique
        # [6] Reads w/ downstream anchor
        # [7] " unique
        # [8] Length

# 1) Distribution of deletion sizes
max_size_found = 0
del_list = list()
del_size_dist = dict()
for sv in deletions:
    if (sv[5] < min_reads) or (sv[7] < min_reads):
        continue
    del_size = sv[8]
    if (del_size < min_del_size):
        continue
    if del_size not in del_size_dist:
        del_size_dist[del_size] = 0
    if max_size_found < del_size: max_size_found = del_size
    del_size_dist[del_size] += 1
    del_list.append(del_size)

# for i in range(min_del_size,max_size_found):
#     if i not in del_size_dist:
#         del_size_dist[i] = 0

print("Min supporting reads: {min_reads}".format(min_reads=min_reads), file=sys.stderr)
print("        Min del size: {min_size}".format(min_size=min_del_size), file=sys.stderr)
print("-----------", file=sys.stderr)
print("     Dels found: {dels}".format(dels=len(del_list)), file=sys.stderr)
if len(del_list) > 0:
    print("   Max del size: {max}".format(max=max(del_list)), file=sys.stderr)
    print("   Min del size: {min}".format(min=min(del_list)), file=sys.stderr)
    print("  Mean del size: {mean:.1f}".format(mean=numpy.average(numpy.array(del_list))), file=sys.stderr)
    print("Median del size: {median:.1f}".format(median=numpy.median(numpy.array(del_list))), file=sys.stderr)


with open(length_hist_file, "w") as fo:
    for i in range(min_del_size, max_size_found+1):
        if i not in del_size_dist:
            print("{i} 0".format(i=i), file=fo)
        else:
            print("{i} {count}".format(i=i, count=del_size_dist[i]), file=fo)

# 2) Distribution of break points along the segment
min_pos = 0
max_pos = 0
del_start_dist = dict()
del_end_dist = dict()
unique_dels = dict()
unique_ids = dict()
for sv in deletions:
    if (sv[5] < min_reads) or (sv[7] < min_reads):
        continue
    del_size = sv[3][0] - sv[2][0]
    if (del_size < min_del_size):
        continue
    r_size = sv[2][1] - sv[2][0] + 1
    for i in range(sv[2][0], sv[2][1]+1):
        if i not in del_start_dist:
            del_start_dist[i] = 0
        if min_pos == 0: min_pos = i
        if min_pos > i: min_pos = i
        del_start_dist[i] += 1/r_size

    for j in range(sv[3][0], sv[3][1]+1):
        if i not in del_end_dist:
            del_end_dist[i] = 0
        if max_pos < i: max_pos = i
        del_end_dist[i] += 1/r_size

    del_signature = str(sv[2][0]) + "-" + str(sv[2][1]) + " " + str(sv[3][0]) + "-" + str(sv[3][1])
    if del_signature not in unique_dels:
        unique_dels[del_signature] = sv[4] + sv[6]
        unique_ids[del_signature] = sv[0]
    else:
        print("Repeated deletion: {a} vs {b}".format(a=unique_ids[del_signature], b=sv[0]), file=sys.stderr)
        print("del_signature={a}".format(a=del_signature), file=sys.stderr)
        print(sv, file=sys.stderr)
        unique_dels[del_signature] += sv[4] + sv[6]

for i in range(min_pos, max_pos+1):
    if i not in del_start_dist:
        del_start_dist[i] = 0

for i in range(min_pos, max_pos+1):
    if i not in del_end_dist:
        del_end_dist[i] = 0

with open(start_hist_file, "w") as fo:
    for i in range(min_pos, max_pos+1):
        if i not in del_start_dist:
            print("{i} 0".format(i=i), file=fo)
        else:
            print("{i} {count}".format(i=i, count=del_start_dist[i]), file=fo)

with open(end_hist_file, "w") as fo:
    for i in range(min_pos, max_pos+1):
        if i not in del_end_dist:
            print("{i} 0".format(i=i), file=fo)
        else:
            print("{i} {count}".format(i=i, count=del_end_dist[i]), file=fo)
            
with open(BP_file, "w") as fo:
    for i in unique_dels:
        print("{d} {r}".format(d=i, r=unique_dels[i]), file=fo)

