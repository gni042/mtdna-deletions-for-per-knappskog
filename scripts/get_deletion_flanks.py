#!/usr/bin/python3

import os
import sys
import numpy
import re
import math
import gzip
import pysam
import statistics
#import csv

if (len(sys.argv)!=6):
    print("USAGE: {program} {pindelfile} {bamfile} {min_reads} {min_del_length} {mt_chr}".format(program=sys.argv[0],
          pindelfile="<pindel_outputfile_D>", bamfile="<bamfile.bam>",
          min_reads="MIN_SUPPORTING_READS", min_del_length="MIN_DELETION_LENGTH", mt_chr="<MT_CHR_NAME>", file=sys.stderr))
    sys.exit(1)

deletions_file = sys.argv[1]
bam_file = sys.argv[2]

if not os.path.isfile(deletions_file):
    print("File \"{file}\" does not exist. Abort.".format(file=deletions_file), file=sys.stderr)
    sys.exit(1)
if not os.path.isfile(bam_file):
    print("File \"{file}\" does not exist. Abort.".format(file=bam_file), file=sys.stderr)
    sys.exit(1)


###############################################################################
#   PARAMETERS                                                                #
###############################################################################

# Number of upstream-anchored and downstream anchored supporting reads required
min_reads = int(sys.argv[3])
# Minimum deletion size
min_del_size = int(sys.argv[4])
# Name of mt chromosome in reference
mtchr = sys.argv[5]


###############################################################################
#   OUTPUT FILES                                                              #
###############################################################################

# Summarising table
out_file = deletions_file + "_flanking_seqs_" + str(min_reads) + "_" + str(min_del_size) + ".csv"

# List of regions
regions_file = deletions_file + "_flanking_regions_" + str(min_reads) + "_" + str(min_del_size) + ".bed"

# List of split reads
splitreads_file = deletions_file + "_splitreads_" + str(min_reads) + "_" + str(min_del_size) + ".txt"

print("Output will be written in:\n{f1}".format(f1=out_file), file=sys.stderr)
print("Regions BED will be written in:\n{f2}".format(f2=regions_file), file=sys.stderr)
print("List of split reads will be written in:\n{f3}".format(f3=splitreads_file), file=sys.stderr)
print("-----------", file=sys.stderr)


###############################################################################
#   FUNCTIONS                                                                 #
###############################################################################

def group_by_heading( source_file ):
    buffer = []
    for line in source_file:
        if line.startswith( "##########" ):
            if buffer: yield buffer
            buffer = [ line.strip() ]
        else:
            buffer.append( line.strip() )
    yield buffer

#def mean_seq_qual( ascii_seq ):
#    #quals = [ ord(letter)-33 for letter in ascii_seq ]
#    return -10 * math.log(sum([ 10**(q / -10) for q in [ ord(letter)-33 for letter in ascii_seq ] ]) / len(ascii_seq), 10)

def read_bam( bamfilename ):
    bamfile = pysam.AlignmentFile(bamfilename, 'rb', threads=8)
    Reads = dict()
    read1 = None
    read2 = None
    unpaired = 0
    r_num = 0
    for read in bamfile:
        r_num += 1
        if not read.is_paired:
            unpaired += 1
            print("Read {} in line {} is unpaired".format(read.query_name, r_num))
            continue
        if read.is_read2:
            read2 = read
        else:
            read1 = read
            read2 = None
            continue
        #print("Current situation: {}/1 VS {}/2".format(read1.query_name, read2.query_name))
        if not read1 is None and not read2 is None:
            if read1.query_name != read2.query_name:
                print("QUERYNAMES OF PAIRS ARE DIFFERENT in {}: {} / {}".format(r_num, read1.query_name, read2.query_name))
                continue
            if not read1.reference_name == mtchr and not read2.reference_name == mtchr:
                #print("None of the reads are mapped to {}".format(mtchr))
                continue
            readname1 = read1.query_name + '/1'
            readname2 = read2.query_name + '/2'
            mm_prop1 = 1
            mm_prop2 = 1
            if not read1.is_unmapped and read1.reference_name == mtchr:
                cigar_stats = [x for x in read1.get_cigar_stats()[0]]
                read_nb_positions = cigar_stats[0] + cigar_stats[1] + cigar_stats[7] + cigar_stats[8]
                mismatches_and_insertions = cigar_stats[1] + cigar_stats[10]
                mm_prop1 = mismatches_and_insertions / read_nb_positions
            if not read2.is_unmapped and read2.reference_name == mtchr:
                cigar_stats = [x for x in read2.get_cigar_stats()[0]]
                read_nb_positions = cigar_stats[0] + cigar_stats[1] + cigar_stats[7] + cigar_stats[8]
                mismatches_and_insertions = cigar_stats[1] + cigar_stats[10]
                mm_prop2 = mismatches_and_insertions / read_nb_positions
            is_good_pair = False
            if not read1.is_unmapped and not read2.is_unmapped and read1.reference_name == read2.reference_name:
                if not read1.is_reverse and read2.is_reverse:
                    if read1.reference_start < read2.reference_start:
                        is_good_pair = True
                if read1.is_reverse and not read2.is_reverse:
                    if read1.reference_start > read2.reference_start:
                        is_good_pair = True
            if readname1 in Reads or readname2 in Reads:
                print("WARNING: read with several hits: {} {}".format(readname1, readname2), file=sys.stderr)
            else:
                Reads[readname1] = [ read1.mapping_quality, statistics.median(read1.query_alignment_qualities), mm_prop1, read2.mapping_quality, statistics.median(read2.query_alignment_qualities), mm_prop2, is_good_pair ]
                Reads[readname2] = [ read2.mapping_quality, statistics.median(read2.query_alignment_qualities), mm_prop2, read1.mapping_quality, statistics.median(read1.query_alignment_qualities), mm_prop1, is_good_pair ]
    bamfile.close()
    return(Reads)



###############################################################################
#   BODY                                                                      #
###############################################################################

deletions = list()

try:
    of = open(out_file, "w")
except:
    print("Something went wrong while trying to open {f} for writing".format(f=out_file), file=sys.stderr)

try:
    of_r = open(regions_file, "w")
except:
    print("Something went wrong while trying to open {f} for writing".format(f=regions_file), file=sys.stderr)

try:
    srf_r = open(splitreads_file, "w")
except:
    print("Something went wrong while trying to open {f} for writing".format(f=splitreads_file), file=sys.stderr)

## Headers for output files
print("id position A C G T del_start del_end del_size supports", file=of)
print("del_id read_id del_start1 del_start2 del_end1 del_end2 del_size supports pindel_mapq mapq median_seq_qual prop_mism mate_mapq mate_median_seq_qual mate_prop_mism good_pair", file=srf_r)


## Read read records from bam file

print("Reading bamfile...", file=sys.stderr)
read_attrs = read_bam(bam_file)
print("DONE!", file=sys.stderr)
#read_quals = read_qual_from_fastq(fastq_file1, append='/1')
#read_quals.extend(read_qual_from_fastq(fastq_file2, append='/2'))
#read_quals = dict(read_quals)


a = 0
split_reads = dict()
with open( deletions_file, "r" ) as f:
    for subheader in group_by_heading( f ):
        sh = subheader[1].split()
        if sh[1] != "D":
            print("Non-deletion event found: {sv}\nAborting.".format(sv=subheader[1]), file=sys.stderr)
            sys.exit(1)
        del_ID      = str(sh[0])
        del_len     = int(sh[2])
        del_support = int(sh[16])
        #start_point
        del_start   = int(sh[9])
        #end_point
        del_end     = int(sh[10])
        start_R     = int(sh[12])
        end_R       = int(sh[13])
        if int(sh[4]) > 0:
            continue
        if del_support < min_reads:
            continue
        if del_len < min_del_size:
            continue
        start_diff = del_start - start_R
        end_diff = end_R - del_end
        range_start = [ start_R, del_start + end_diff ]
        range_end   = [ del_end - start_diff, end_R ]
        #print(sh)
        deletion = dict()
        nucs = dict()
        read_ids = set()
        pindel_mapqs = dict()
        l_min = del_start
        r_max = del_end
        for i in range(3, len(subheader)):
            read = subheader[i].split()
            L = list(read[0])
            st = del_start - len(L) + 1
            en = del_start
            if st < l_min: l_min = st
            pos = st
            for nuc in L:
                if nuc not in nucs: nucs[nuc] = dict()
                if pos not in nucs[nuc]: nucs[nuc][pos] = 0
                nucs[nuc][pos] += 1
                pos += 1
            R = read[1]
            st = del_end
            en = del_end + len(R) - 1
            if en > r_max: r_max = en
            pos = st
            for nuc in R:
                if nuc not in nucs: nucs[nuc] = dict()
                if pos not in nucs[nuc]: nucs[nuc][pos] = 0
                nucs[nuc][pos] += 1
                pos += 1
            r_name = re.sub('^@', '', read[6])
            #r_name = re.sub('/\d$', '', r_name)
            read_ids.add(r_name)
            pindel_mapqs[r_name] = int(read[4])
        # Left-flanking region
        for j in range(l_min, del_start+1):
            if j not in nucs['A']: nucs['A'][j] = 0
            if j not in nucs['C']: nucs['C'][j] = 0
            if j not in nucs['G']: nucs['G'][j] = 0
            if j not in nucs['T']: nucs['T'][j] = 0
            print("{d} {pos} {A} {C} {G} {T} {start} {end} {size} {depth}".format(d=del_ID,
                   pos=j, A=nucs['A'][j], C=nucs['C'][j], G=nucs['G'][j], T=nucs['T'][j],
                   start=del_start, end=del_end, size=del_len, depth=del_support), file=of)
        # Right-flanking region
        for j in range(del_end, r_max+1):
            if j not in nucs['A']: nucs['A'][j] = 0
            if j not in nucs['C']: nucs['C'][j] = 0
            if j not in nucs['G']: nucs['G'][j] = 0
            if j not in nucs['T']: nucs['T'][j] = 0
            print("{d} {pos} {A} {C} {G} {T} {start} {end} {size} {depth}".format(d=del_ID,
                   pos=j, A=nucs['A'][j], C=nucs['C'][j], G=nucs['G'][j], T=nucs['T'][j],
                   start=del_start, end=del_end, size=del_len, depth=del_support), file=of)
        print("{mtchr}\t{s}\t{e}".format(mtchr=mtchr, s=l_min, e=del_start), file=of_r)
        print("{mtchr}\t{s}\t{e}".format(mtchr=mtchr, s=del_end, e=r_max), file=of_r)
        for k in read_ids:
            if k not in read_attrs:
                sys.exit("Read was not found in original alignment!")
                #print("{del_id} {r} {start1} {start2} {end1} {end2} {size} {depth} -1 0.00 0.000 False".format(del_id=del_ID, r=k,
                #       start1=range_start[0], start2=range_start[1], end1=range_end[0], end2=range_end[1], size=del_len, depth=del_support), file=srf_r)
            else:
                read_chars = read_attrs[k]
                split_reads[k] = read_chars
                print("{del_id} {r} {start1} {start2} {end1} {end2} {size} {depth} {pindel_mapq:d} {mapq:d} {mean_qual:.2f} {prop_mism:.4f} {mate_mapq:d} {mate_mean_qual:.2f} {mate_prop_mism:.4f} {good_pair}".format(del_id=del_ID, r=k,
                       start1=range_start[0], start2=range_start[1], end1=range_end[0],
                       end2=range_end[1], size=del_len, depth=del_support,
                       pindel_mapq=pindel_mapqs[k], mapq=read_chars[0],
                       mean_qual=read_chars[1], prop_mism=read_chars[2],
                       mate_mapq=read_chars[3], mate_mean_qual=read_chars[4],
                       mate_prop_mism=read_chars[5], good_pair=read_chars[6]),
                       file=srf_r)

of.close()
of_r.close()
srf_r.close()
