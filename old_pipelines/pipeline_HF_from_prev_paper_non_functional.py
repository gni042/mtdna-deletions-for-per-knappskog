#!/bin/bash

#### FILES #####
valid_ids="/data1/per_mtdna/ids"
reference_genome="/ref/genomes/b37_decoy/b37_decoy.fasta"
illumina_adapters="/opt/Trimmomatic-0.36/adapters/TruSeq3-SE.fa"
region_of_interest="/data1/per_mtdna/region_of_interest.bed"

#### VARIABLES ####
mtchr="MT"
insert_size=340

#### FOLDERS TO GENERATE ####
mkdir -p /data1/per_mtdna/results/trimmed_fq
#mkdir -p /data1/per_mtdna/results/fastqfiles_no-adaptors
mkdir -p /data1/per_mtdna/results/logs
mkdir -p /data1/per_mtdna/results/bamfiles
mkdir -p /data1/per_mtdna/results/insertsizes
mkdir -p /data1/per_mtdna/results/contigs
mkdir -p /data1/per_mtdna/results/vcffiles
mkdir -p /data1/per_mtdna/results/qc
mkdir -p /data1/per_mtdna/results/pindel_configs_strict
mkdir -p /data1/per_mtdna/results/pindel_output_strict
#mkdir -p /data1/per_mtdna/results/depth
#mkdir -p /data1/per_mtdna/results/permutations
#mkdir -p /data1/per_mtdna/results/tmp
mkdir -p /data1/per_mtdna/tmp

#### PROGRAMS ####
trimmomatic_jar="/opt/Trimmomatic-0.39/trimmomatic-0.39.jar"
samtools_exe="/opt/samtools-1.9/samtools"
bwa_exe="/usr/local/bin/bwa-0.7.17"
gatk_jar="/opt/GenomeAnalysisTK-3.8.1/GenomeAnalysisTK.jar"
picard_jar="/opt/PicardTools-2.21.1/picard.jar"
# PINDEL THAT WORKS: https://github.com/genome/pindel/archive/70c1bb4a75503da39e206e02178fe3d8a0afdf81.tar.gz
pindel_exe="/opt/pindel-0.2.5b9/pindel"
bedtools_exe="/opt/bedtools-2.29.0/bin/bedtools"
bam_readcount_exe="/opt/bam-readcount-0.8.0/bam-readcount"
# https://github.com/ding-lab/Pindel2BAM
#pindel2sam_exe="/opt/Pindel2BAM/pin2sam"
# This version here does not remove the /1 or /2 of the read ids:
pindel2sam_exe="/data1/per_mtdna/scripts/pin2sam"
get_deletion_flanks_script="/data1/per_mtdna/scripts/get_deletion_flanks.py"
summarise_deletions_script="/data1/per_mtdna/scripts/summarise_deletions.py"
remove_small_deletions_script="/data1/per_mtdna/scripts/remove_small_deletions.py"


###############################################################################
#   TRIMMING                                                                  #
###############################################################################

echo "( 1) TRIMMING"

for id in `cat ${valid_ids}`; do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz"
    out2="/data1/per_mtdna/results/trimmed_fq/${id}_unpaired.R1.fastq.gz"
    out3="/data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz"
    out4="/data1/per_mtdna/results/trimmed_fq/${id}_unpaired.R2.fastq.gz"
    if [ -s "${out1}" ] && [ -s "${out2}" ] && [ -s "${out3}" ] && [ -s "${out4}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "     Trimming!"
        java -jar ${trimmomatic_jar} PE \
            /data1/per_mtdna/fastqfiles/${id}.R1.fastq.gz \
            /data1/per_mtdna/fastqfiles/${id}.R2.fastq.gz \
        	${out1} \
	    	${out2} \
            ${out3} \
            ${out4} \
	    	ILLUMINACLIP:${illumina_adapters}:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 \
            2> /data1/per_mtdna/results/logs/${id}_trimming.stderr
    fi
done

###############################################################################
#   ALIGNING WITH BWA                                                         #
###############################################################################

echo "( 2) ALIGNING WITH BWA"

for id in `cat ${valid_ids}`; do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/bamfiles/${id}.bam"
    out2="/data1/per_mtdna/results/bamfiles/${id}.bam.bai"
    if [ -s "${out1}" ] && [ -s "${out2}" ]
    then
        echo "     Output files already present, skipping"
    else
        ${bwa_exe} mem -t 16 \
            -R "@RG\tID:foo\tSM:bar" \
            ${reference_genome} \
            /data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz \
            /data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz \
        2> /data1/per_mtdna/results/logs/bwa1_${id}.stderr | \
        ${samtools_exe} view -b -h -@ 16 - | \
        ${samtools_exe} sort -@ 16 -o ${out1} -
        ${samtools_exe} index ${out1}
    fi
done

###############################################################################
#   CALCULATING mtDNA EFFICIENCY AND INSERT SIZES                             #
###############################################################################

echo "MAPPING EFFICIENCY..."

for id in `cat ${valid_ids}`
do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/insertsizes/${id}.txt"
    out2="/data1/per_mtdna/results/insertsizes/${id}.pdf"
    if [ -s "${out1}" ] && [ -s "${out2}" ]
    then
        echo "     Output files already present, skipping"
    else
        java -jar /opt/picard-2.6.0/picard.jar CollectInsertSizeMetrics \
            I=/data1/per_mtdna/results/bamfiles/${id}.bam \
            O=${out1} \
            H=${out2} \
            2> /data1/per_mtdna/results/logs/mapping_eff.stderr
    fi
done

out1="/data1/per_mtdna/results/mt_vs_total_reads.tsv"
if [ ! -s "${out1}" ]
then
    for id in `cat ${valid_ids}`
    do
        ALL=$( ${samtools_exe} view -F 4 -q 30 -c /data1/per_mtdna/results/bamfiles/${id}.bam )
        MT=$( ${samtools_exe} view -F 4 -q 30 -c /data1/per_mtdna/results/bamfiles/${id}.bam ${mtchr} )
        echo -e "${id}\t${MT}\t${ALL}"
    done > ${out1}
fi


###############################################################################
#   CREATING NEW CONTIG TO REALIGN                                            #
###############################################################################

echo "( 4) CREATING NEW CONTIG AND REALIGNING"
 
for id in `cat ${valid_ids}`; do
    echo "     ${id}"
    out1="/data1/per_mtdna/results/bamfiles/${id}_dedup.bam"
    out2="/data1/per_mtdna/results/qc/${id}_dup_metrics.txt"
    out3="/data1/per_mtdna/results/bamfiles/${id}_dedup.bam.bai"
    out4="/data1/per_mtdna/results/vcffiles/${id}.vcf"
    out5="/data1/per_mtdna/results/vcffiles/${id}_filtered.vcf"
    out6="/data1/per_mtdna/results/contigs/${id}.fa"
    realigned_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam"
    out8="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam.bai"
    nonclipped_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam"
    if [ -s "${out5}" ] && [ -s "${realigned_bam}" ] && [ -s "${out8}" ] && [ -s "${nonclipped_bam}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "      (a) Marking duplicates"
        java -jar ${picard_jar} MarkDuplicates \
            I=/data1/per_mtdna/results/bamfiles/${id}.bam \
            O=${out1} \
            M=${out2} \
            2> /data1/per_mtdna/results/logs/MarkDuplicates_${id}.stderr
        ${samtools_exe} index /data1/per_mtdna/results/bamfiles/${id}_dedup.bam

        echo "      (b) Finding SNPs in ${mtchr} chromosome"
        java -jar ${gatk_jar} \
            -R ${reference_genome} \
            -T HaplotypeCaller \
            -ploidy 1 \
            -L ${mtchr} \
            -I ${out1} \
            -o ${out4} \
        2> /data1/per_mtdna/results/logs/HaplotypeCaller_${id}.stderr > /data1/per_mtdna/results/logs/HaplotypeCaller_${id}.stdout

        echo "      (c) Filtering SNPs"
        java -jar ${gatk_jar} \
            -T SelectVariants \
            -R ${reference_genome} \
            -V ${out4} \
            -L ${mtchr} \
            -selectType SNP \
            -select "QD > 10.0 || FS < 20.0 || QUAL > 1000.0" \
            -o ${out5} \
        2> /data1/per_mtdna/results/logs/SelectVariants_${id}.stderr > /data1/per_mtdna/results/logs/SelectVariants_${id}.stdout

        echo "      (d) Creating alternate reference with selected SNPs"
        java -jar ${gatk_jar} \
            -R ${reference_genome} \
            -T FastaAlternateReferenceMaker \
            -o ${out6} \
            -V ${out5} \
        2> /data1/per_mtdna/results/logs/FastaAlternateReferenceMaker_${id}.stdout > /data1/per_mtdna/results/logs/FastaAlternateReferenceMaker_${id}.stderr

        sed "s/^>.*${mtchr}:1$/>${mtchr}/" ${out6} > ${out6}.tmp
        mv ${out6}.tmp ${out6}

        echo "      (e) Indexing alternate reference"
        ${bwa_exe} index ${out6} 2> /data1/per_mtdna/results/logs/bwaIndex_${id}.stderr > /data1/per_mtdna/results/logs/bwaIndex_${id}.stdout

        echo "      (f) Re-aligning to alternate reference"
        ${bwa_exe} mem -t 16 \
            -R "@RG\tID:foo\tSM:bar" \
            ${out6} \
            /data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz \
            /data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz \
        2> /data1/per_mtdna/results/logs/bwa2_${id}.stderr | \
        ${samtools_exe} view -b -h -F 2048 -@ 16 - | \
        ${samtools_exe} sort -@ 16 -o ${realigned_bam} -
        ${samtools_exe} index ${realigned_bam}

        echo "      (g) Removing soft- and hard-clipped reads from alignment"
        ${samtools_exe} view -q 30 -h -@ 16 ${realigned_bam} ${mtchr} | \
            awk -F '\t' '($6 !~ /H|S/)' | \
            ${samtools_exe} view -b -h -@ 16 - \
            > ${nonclipped_bam}
        echo "      (h) Indexing cleaned up alignement"
        ${samtools_exe} index ${nonclipped_bam}
    fi
done

## To merge vcfs into a single multisample vcf,
## bash /data1/per_mtdna/scripts/merge_vcfs.sh
## bash /data1/per_mtdna/scripts/merge_vcfs_filtered.sh
## ... and by hand removed variants outside 5800-14860
#
###############################################################################
#   FINDING DELS WITH PINDEL                                                  #
###############################################################################

mdl=100 # Minimum deletion length

echo "( 5) FINDING DELETIONS WITH PINDEL"

for id in `cat ${valid_ids}`
do
    echo "     ${id}"
    cfg_file="/data1/per_mtdna/results/pindel_configs_strict/${id}.cfg"
    contig_ref="/data1/per_mtdna/results/contigs/${id}.fa"
    fai_index="/data1/per_mtdna/results/contigs/${id}.fa.fai"
    pindel_file="/data1/per_mtdna/results/pindel_output_strict/${id}_D"
    splitreads_bam="/data1/per_mtdna/results/bamfiles/${id}_splitreads_strict.bam"
    if [ -s "${cfg_file}" ] && [ -s "${fai_index}" ] && [ -s "${pindel_file}" ] && [ -s "${splitreads_bam}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "      (a) Running PINDEL"
        echo -e "/data1/per_mtdna/results/bamfiles/${id}_realigned.bam\t${insert_size}\t${id}" \
            > ${cfg_file}
        ${samtools_exe} faidx ${contig_ref}
        ${pindel_exe} -f ${contig_ref} \
            -i ${cfg_file} \
            -c ${mtchr}:1,000-15,000 \
            -T 16 \
            -E 0.75 \
            -a 5 \
            -m 6 \
            -e 0 \
            -x 5 \
            -r false \
            -t false \
            -l false \
            -k true \
            -j ${region_of_interest} \
            -A 30 \
            -L /data1/per_mtdna/results/logs/pindel_strict_${id}.log \
            -o /data1/per_mtdna/results/pindel_output_strict/${id} \
        > /data1/per_mtdna/results/logs/pindel_strict_${id}.stdout 2> /data1/per_mtdna/results/logs/pindel_strict_${id}.stderr
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_BP
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_CloseEndMapped
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_INT_final
        rm /data1/per_mtdna/results/pindel_output_strict/${id}_LI
        echo "      (b) Creating alignment of split reads"
        rm -rf /data1/per_mtdna/tmp/*
        python3.6 ${remove_small_deletions_script} ${pindel_file} ${mdl} > /data1/per_mtdna/tmp/${id}_D
        ${pindel2sam_exe} tmp \
            /data1/per_mtdna/tmp \
            ${cfg_file} \
            ${fai_index} \
            2> /data1/per_mtdna/results/logs/pindel2sam_strict_${id}.stderr \
            > /data1/per_mtdna/results/logs/pindel2sam_strict_${id}.stdout
        OUTFILE=/data1/per_mtdna/tmp/$(cat ${cfg_file} | cut -f1 | sed "s/\//_/g").sam
        ${samtools_exe} view -b -h -@ 16 ${OUTFILE} | \
            ${samtools_exe} sort -@ 16 -o ${splitreads_bam} \
            2> /dev/null
        ${samtools_exe} index ${splitreads_bam}
        rm ${OUTFILE} /data1/per_mtdna/tmp/${id}_D
    fi
done
###############################################################################
#   PARSING PINDEL OUTPUT                                                     #
###############################################################################

mr=1   # Minimum number of reads supporting a deletion
mdl=100 # Minimum deletion length
echo "( 6) PARSING PINDEL OUTPUT (min_reads=${mr}, min_del_length=${mdl})"

for id in `cat ${valid_ids}`
do
    echo "     ${id}"
    pindel_file="/data1/per_mtdna/results/pindel_output_strict/${id}_D"
    bam="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam"
    name_sorted_tmp="/data1/per_mtdna/tmp/${id}_realigned_namesorted.bam"
    contig_ref="/data1/per_mtdna/results/contigs/${id}.fa"
    fai_index="/data1/per_mtdna/results/contigs/${id}.fa.fai"
    cfg_file="/data1/per_mtdna/results/pindel_configs_strict/${id}.cfg"
    out_dels="/data1/per_mtdna/results/pindel_output_strict/${id}_D_dels_${mr}_${mdl}.dat"
    out_flanks="/data1/per_mtdna/results/pindel_output_strict/${id}_D_flanking_regions_${mr}_${mdl}.bed"
    out_flanks_sorted="/data1/per_mtdna/results/pindel_output_strict/${id}_D_flanking_regions_${mr}_${mdl}_sorted.bed"
    realigned_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned.bam"
    nonclipped_bam="/data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam"
    nondeleted_flanks="/data1/per_mtdna/results/pindel_output_strict/${id}_non-deleted_flanks_${mr}_${mdl}.csv"
    if [ -f "${out_dels}" ] && [ -f "${out_flanks}" ] && [ -f "${nondeleted_flanks}" ]
    then
        echo "     Output files already present, skipping"
    else
        echo "      (a) Filtering and tidying up Pindel breakpoints"
        python3.6 ${compute_breakpoints_script} ${pindel_file} ${mr} ${mdl} \
            2> /dev/null
        echo "      (b) Getting deletion flanking regions"
        ${samtools_exe} sort -n -@ 16 -o ${name_sorted_tmp} ${bam} 2> /dev/null
        python3.6 ${get_deletion_flanks_script} ${pindel_file} ${name_sorted_tmp} ${mr} ${mdl} ${mtchr}\
            2> /dev/null
        rm ${name_sorted_tmp}
        sort -n -k 2 ${out_flanks} \
            > ${out_flanks_sorted}
        echo "      (c) Merging interesting areas"
        ${bedtools_exe} merge -i ${out_flanks_sorted} \
            > ${out_flanks}
        rm ${out_flanks_sorted}
        echo "      (d) Bam read counting"
        ${bam_readcount_exe} -q 30 -b 30 -d 5000000 -w 1 \
            -l ${out_flanks} \
            -f ${contig_ref} ${nonclipped_bam} \
            2> /dev/null | cut -f2,6,7,8,9 \
            > ${nondeleted_flanks}
    fi
done

exit

# Generate a summary file with deletions for all samples

echo "   * Generating a summary of all deletion for all samples"
python3.6 ${summarise_deletions_script} /data1/per_mtdna/results/pindel_output_strict/*_D_dels_${mr}_${mdl}.dat \
    > /data1/per_mtdna/results/summary_deletions_${mr}_${mdl}.tsv

exit
#echo "( 7) GENERATING NEW CONTIGS WITH MAIN DELETIONS AND REALIGNING ONCE AGAIN"
#
#ms=1000  # Minimum supporting depth. It's the same as the mr variable used above but stricter
#
#for id in `cat ${valid_ids}`; do
#    echo "     ${id}"
#    seq=$( ${samtools_exe} faidx /data1/per_mtdna/results/contigs/${id}.fa ${mtchr} | \
#        grep -v "^>${mtchr}" | awk '{printf("%s",$0)}' )
#    echo "id del_start del_end del_size supports" \
#        > /data1/per_mtdna/results/pindel_output_strict/${id}_D_list_deletions_${ms}_${mdl}.csv
#    cut -f 1,7,8,9,10 -d ' ' /data1/per_mtdna/results/pindel_output_strict/${id}_D_flanking_seqs_${mr}_${mdl}.csv | \
#        awk -v var="${ms}" '{if($5>var) print $0}' | grep -v '^id' | sort | uniq \
#        >> /data1/per_mtdna/results/pindel_output_strict/${id}_D_list_deletions_${ms}_${mdl}.csv
#    grep -v '^id' /data1/per_mtdna/results/pindel_output_strict/${id}_D_list_deletions_${ms}_${mdl}.csv | \
#        while IFS=" " read -r del_id del_start del_end del_size del_supports; do
#        echo "     del ${del_id} ${del_start} ${del_end}"
#        # Delete bases from seq
#        Lseq=$( echo ${seq:0:del_start} )
#        Rseq=$( echo ${seq:del_end-1} )
#        # TOO SLOW TO ADD THE WHOLE GENOME!
#        #cp /data1/per_mtdna/results/contigs/${id}.fa /data1/per_mtdna/results/contigs/${id}_${del_id}_fullGen.fa
#        echo ">${mtchr}" > /data1/per_mtdna/results/contigs/${id}_${del_id}.fa
#        echo "${seq}" | fold -w 60 >> /data1/per_mtdna/results/contigs/${id}_${del_id}.fa
#        echo ">${mtchr}_${del_id}" >> /data1/per_mtdna/results/contigs/${id}_${del_id}.fa
#        echo "${Lseq}${Rseq}" | fold -w 60 >> /data1/per_mtdna/results/contigs/${id}_${del_id}.fa
#        # Index for bwa
#        ${bwa_exe} index /data1/per_mtdna/results/contigs/${id}_${del_id}_fullGen.fa \
#            2> /data1/per_mtdna/results/logs/bwaIndex_${id}_${del_id}.stderr \
#            > /data1/per_mtdna/results/logs/bwaIndex_${id}_${del_id}.stdout
#            # Re-align
#        ${bwa_exe} mem -t 16 \
#            -R "@RG\tID:foo\tSM:bar" \
#            /data1/per_mtdna/results/contigs/${id}_${del_id}.fa \
#            /data1/per_mtdna/results/trimmed_fq/${id}.R1.fastq.gz \
#            /data1/per_mtdna/results/trimmed_fq/${id}.R2.fastq.gz \
#            2> /data1/per_mtdna/results/logs/bwa3_${id}.stderr | \
#        awk -F '\t' '($6 !~ /H|S/)' | \
#        ${samtools_exe} view -b -h -q 30 -@ 16 - | \
#        ${samtools_exe} sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads.bam -
#        ${samtools_exe} index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads.bam
# HASTA AQUI HE LLEGADO, BASTA CON COMPARAR LOS READS EN EL ALIGNMENT! TIENE LAS REFERNCIAS NORMALS Y DELETED
 #        # Get subset of reads that overlap the deletion point from the alignment
 #        # with the mutant mtDNA
# #        samtools-1.9 view -h -@ 16 /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads.bam | \
# #        awk -v var="${del_start}" -F '\t' '{if ($1 ~ /^@/){print $0} else if ($4 < var-5 && length($10)+$4 > var+5){print $0}}' | \
# #        samtools-1.9 view -@ 16 -b -h - | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam - 
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam
# #        # Get subset of reads that overlap the deletion breakpoints from the alignment
# #        # with the wild type mtDNA
# #        samtools-1.9 view -h -@ 16 /data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam | \
# #        awk -v del_start="${del_start}" -v del_end="${del_end}" -F '\t' '{if ($1 ~ /^@/){print $0} else if ($4 < del_start-5 && length($10)+$4 > del_start+5){print $0} else if ($4 < del_end-5 && length($10)+$4 > del_end+5){print $0}}' | \
# #        samtools-1.9 view -@ 16 -b -h - | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.bam - 
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.bam
# #
# #        # Get the list of reads in both ref and mut
# #        comm -12 \
# #            <( samtools-1.9 view -@ 16 results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam | \
# #                cut -f1 | sort ) \
# #            <( samtools-1.9 view -@ 16 results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.bam | \
# #                cut -f1 | sort) \
# #                > /data1/per_mtdna/results/bamfiles/tmp.list
# #
# #        # Separate reads from L breakpoints REFERENCE
# #        samtools-1.9 view -h -@ 16 /data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam | \
# #        awk -v del_start="${del_start}" -F '\t' '{if ($1 ~ /^@/){print $0} else if ($4 < del_start-5 && length($10)+$4 > del_start+5){print $0}}' | \
# #        awk -F '\t' 'FNR==NR {a[$1]; next} !($1 in a)' /data1/per_mtdna/results/bamfiles/tmp.list "-" | \
# #        samtools-1.9 view -@ 16 -b -h - | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_L.bam - 
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_L.bam
# #        # Separate reads from R breakpoints REFERENCE
# #        samtools-1.9 view -h -@ 16 /data1/per_mtdna/results/bamfiles/${id}_realigned_no-clipped-reads.bam | \
# #        awk -v del_end="${del_end}" -F '\t' '{if ($1 ~ /^@/){print $0} else if ($4 < del_end-5 && length($10)+$4 > del_end+5){print $0}}' | \
# #        awk -F '\t' 'FNR==NR {a[$1]; next} !($1 in a)' /data1/per_mtdna/results/bamfiles/tmp.list "-" | \
# #        samtools-1.9 view -@ 16 -b -h - | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_R.bam - 
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_R.bam
# #        # Remove common reads from L+R alignment and overwrite it
# #        samtools-1.9 view -h -@ 16 /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.bam | \
# #        awk -F '\t' 'FNR==NR {a[$1]; next} !($1 in a)' /data1/per_mtdna/results/bamfiles/tmp.list "-" | \
# #        samtools-1.9 view -@ 16 -b -h - | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.tmp.bam -
# #        mv /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.tmp.bam /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.bam
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping.bam
# #
# #        # Remove common reads from mutant alignment and overwrite it (MUTANT)
# #        samtools-1.9 view -h -@ 16 /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam | \
# #        awk -F '\t' 'FNR==NR {a[$1]; next} !($1 in a)' /data1/per_mtdna/results/bamfiles/tmp.list "-" | \
# #        samtools-1.9 view -@ 16 -b -h - | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.tmp.bam -
# #        mv /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.tmp.bam /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam
# #
# #        # Merge ref_L + mut for subsampling
# #        samtools-1.9 merge -@ 16 \
# #            -f -h /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_L.bam - \
# #            /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_L.bam \
# #            /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_ref-and-L_merged.bam 
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_ref-and-L_merged.bam
# #        # Merge ref_R + mut for subsampling
#         samtools-1.9 view -@ 16 -h /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam | \
#             awk -F '\t' -v del_size="${del_size}" 'BEGIN{OFS="\t";} {if($0~/^@/){print $0} else {print $1, $2, $3, $4+del_size, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16}}' | \
#         samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping_IGV-R.bam
#         samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping_IGV-R.bam
# #        samtools-1.9 merge -@ 16 \
# #            -f -h /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_R.bam - \
# #            /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_R.bam \
# #            /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping_IGV-R.bam | \
# #        samtools-1.9 sort -@ 16 -o /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_ref-and-R_merged.bam 
# #        samtools-1.9 index /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_ref-and-R_merged.bam
# #        
# #        # For IGV, we can check the reference with
# #        #   {sample}_delID_{deletion}_reference_no-clipped-reads_only-overlapping.bam
# #        # For the mutant LEFT
# #        #   {sample}_delID_{deletion}_no-clipped-reads_only-overlapping.bam
# #        # For the mutant RIGHT, however, must shift reads
# #        #   {sample}_delID_{deletion}_no-clipped-reads_only-overlapping_IGV-R.bam
# #
#     done
# done
# 
# # echo "( 7) QUANTIFYING HF AND PERMUTING"
# # 
# # ms=1000
# # min_d=1000 # A minimum depth of 1000 reads is not too restrictive and allows to detect 1% heteroplasmy
# #            # with 10 reads of the "minor allele"
# # permutations=1000 # Number of permutations to carry
# # 
# # for id in `cat ${valid_ids}`; do
# #     echo "     ${id}"
# # 
# #     grep -v '^id' /data1/per_mtdna/results/pindel_output_strict/${id}_D_list_deletions_${ms}_${mdl}.csv | \
# #         while IFS=" " read -r del_id del_start del_end del_size del_supports; do
# #         echo "     del ${del_id} ${del_start} ${del_end}"
# #         echo -e "${mtchr}\t$((del_start-500))\t$((del_start+500))" > /data1/per_mtdna/tmp/temp_L.bed
# #         echo -e "${mtchr}\t$((del_end-500))\t$((del_end+500))" > /data1/per_mtdna/tmp/temp_R.bed
# #         # HF for depths per base
# #         # ( A   ) LEFT SIDE OF BREAKPOINT
# #         # ( A.1 ) MUTANT
# #         echo "pos A C G T depth hf" > /data1/per_mtdna/results/depth/${id}_delID_${del_id}_mut_L.csv
# #         bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #             -l /data1/per_mtdna/tmp/temp_L.bed \
# #             /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam 2> /dev/null | \
# #             cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 <= del_start){print $0}}' | \
# #             awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #             awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $2, $3, $4, $5, $6, max/$6}' \
# #             >> /data1/per_mtdna/results/depth/${id}_delID_${del_id}_mut_L.csv \
# #             2> /dev/null
# #         # ( A.2 ) REFERENCE
# #         echo "pos A C G T depth hf" > /data1/per_mtdna/results/depth/${id}_delID_${del_id}_ref_L.csv
# #         bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #             -l /data1/per_mtdna/tmp/temp_L.bed \
# #             /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_L.bam 2> /dev/null | \
# #             cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 <= del_start){print $0}}' | \
# #             awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #             awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $2, $3, $4, $5, $6, max/$6}' \
# #             >> /data1/per_mtdna/results/depth/${id}_delID_${del_id}_ref_L.csv \
# #             2> /dev/null
# #         # ( B   ) RIGHT SIDE OF BREAKPOINT
# #         # ( B.1 ) MUTANT
# #         echo "pos A C G T depth hf" > /data1/per_mtdna/results/depth/${id}_delID_${del_id}_mut_R.csv
# #         bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #             -l /data1/per_mtdna/tmp/temp_L.bed \
# #             /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam 2> /dev/null | \
# #             cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 > del_start){print $0}}' | \
# #             awk -v del_size="${del_size}" '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1+del_size, A[2], C[2], G[2], T[2], $2}' | \
# #             awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $2, $3, $4, $5, $6, max/$6}' \
# #             >> /data1/per_mtdna/results/depth/${id}_delID_${del_id}_mut_R.csv \
# #             2> /dev/null
# #         # ( B.2 ) REFERENCE
# #         echo "pos A C G T depth hf" > /data1/per_mtdna/results/depth/${id}_delID_${del_id}_ref_R.csv
# #         bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #             -l /data1/per_mtdna/tmp/temp_R.bed \
# #             /data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_R.bam 2> /dev/null | \
# #             cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_end="${del_end}" '{if ($2 >= min_d && $1 >= del_end){print $0}}' | \
# #             awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #             awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $2, $3, $4, $5, $6, max/$6}' \
# #             >> /data1/per_mtdna/results/depth/${id}_delID_${del_id}_ref_R.csv \
# #             2> /dev/null
# # 
# #         for perm in `seq 1 ${permutations}`; do
# #             echo -ne "\r       ${perm}/${permutations}"
# #             ### PERMUTATIONS LEFT
# #             mut_file="/data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_no-clipped-reads_only-overlapping.bam"
# #             ref_file_L="/data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_L.bam"
# #             merge_file_L="/data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_ref-and-L_merged.bam"
# #             mut_reads=$( samtools-1.9 view -@ 16 ${mut_file} | wc -l )
# #             ref_reads_L=$( samtools-1.9 view -@ 16 ${ref_file_L} | wc -l )
# #             samtools-1.9 view -@ 16 ${merge_file_L} | shuf > /data1/per_mtdna/tmp/shuf_L.sam
# #             ( samtools-1.9 view -H ${merge_file_L}; head -${mut_reads} /data1/per_mtdna/tmp/shuf_L.sam ) | \
# #             samtools-1.9 view -@ 16 -h - | samtools-1.9 sort -@ 16 -o /data1/per_mtdna/tmp/mut_L.bam -
# #             samtools-1.9 index /data1/per_mtdna/tmp/mut_L.bam
# #             bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #                 -l /data1/per_mtdna/tmp/temp_L.bed \
# #                 /data1/per_mtdna/tmp/mut_L.bam 2> /dev/null | \
# #                 cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 <= del_start){print $0}}' | \
# #                 awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #                 awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $6, max/$6}' \
# #                 > /data1/per_mtdna/tmp/mut_L.csv
# # 
# #             ( samtools-1.9 view -H ${merge_file_L}; tail -${ref_reads_L} /data1/per_mtdna/tmp/shuf_L.sam ) | \
# #             samtools-1.9 view -@ 16 -h - | samtools-1.9 sort -@ 16 -o /data1/per_mtdna/tmp/ref_L.bam -
# #             samtools-1.9 index /data1/per_mtdna/tmp/ref_L.bam
# #             bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #                 -l /data1/per_mtdna/tmp/temp_L.bed \
# #                 /data1/per_mtdna/tmp/ref_L.bam 2> /dev/null | \
# #                 cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 <= del_start){print $0}}' | \
# #                 awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #                 awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $6, max/$6}' \
# #                 > /data1/per_mtdna/tmp/ref_L.csv
# # 
# #             join -j 1 /data1/per_mtdna/tmp/mut_L.csv /data1/per_mtdna/tmp/ref_L.csv -o 1.1,1.3,2.3 | \
# #                 awk '{print $1,$2/$3}' > /data1/per_mtdna/results/permutations/${id}_${del_id}_mut-ref-ratio_${perm}_L.csv
# #             
# #             ### PERMUTATIONS RIGHT
# #             ref_file_R="/data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_reference_no-clipped-reads_only-overlapping_R.bam"
# #             merge_file_R="/data1/per_mtdna/results/bamfiles/${id}_delID_${del_id}_ref-and-R_merged.bam"
# #             ref_reads_R=$( samtools-1.9 view -@ 16 ${ref_file_R} | wc -l )
# #             # NOTE THAT HERE WE ARE MERGIN READS TO THE LEFT FROM THE MUTANT! WE MUST ADD LENGTH OF DEL TO POSITION
# #             samtools-1.9 view -@ 16 ${merge_file_R} | shuf > /data1/per_mtdna/tmp/shuf_R.sam
# #             ( samtools-1.9 view -H ${merge_file_R}; head -${mut_reads} /data1/per_mtdna/tmp/shuf_R.sam ) | \
# #             samtools-1.9 view -@ 16 -h - | samtools-1.9 sort -@ 16 -o /data1/per_mtdna/tmp/mut_R.bam -
# #             samtools-1.9 index /data1/per_mtdna/tmp/mut_R.bam
# #             bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #                 -l /data1/per_mtdna/tmp/temp_R.bed \
# #                 /data1/per_mtdna/tmp/mut_R.bam 2> /dev/null | \
# #                 cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 > del_start){print $0}}' | \
# #                 awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #                 awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $6, max/$6}' \
# #                 > /data1/per_mtdna/tmp/mut_R.csv
# # 
# #             ( samtools-1.9 view -H ${merge_file_R}; tail -${ref_reads_R} /data1/per_mtdna/tmp/shuf_R.sam ) | \
# #             samtools-1.9 view -@ 16 -h - | samtools-1.9 sort -@ 16 -o /data1/per_mtdna/tmp/ref_R.bam -
# #             samtools-1.9 index /data1/per_mtdna/tmp/ref_R.bam
# #             bam-readcount -d 500000 -f /data1/per_mtdna/results/contigs/${id}.fa -w 0 \
# #                 -l /data1/per_mtdna/tmp/temp_R.bed \
# #                 /data1/per_mtdna/tmp/ref_R.bam 2> /dev/null | \
# #                 cut -f2,4,6,7,8,9 | awk -v min_d="${min_d}" -v del_start="${del_start}" '{if ($2 >= min_d && $1 >= del_end){print $0}}' | \
# #                 awk '{split($3,A,":"); split($4,C,":"); split($5,G,":"); split($6,T,":"); print $1, A[2], C[2], G[2], T[2], $2}' | \
# #                 awk '{max=$2;for(i=2;i<=5;i++){if($i > max) max = $i}print $1, $6, max/$6}' \
# #                 > /data1/per_mtdna/tmp/ref_R.csv
# # 
# #             join -j 1 /data1/per_mtdna/tmp/mut_R.csv /data1/per_mtdna/tmp/ref_R.csv -o 1.1,1.3,2.3 | \
# #                 awk '{print $1,$2/$3}' > /data1/per_mtdna/results/permutations/${id}_${del_id}_mut-ref-ratio_${perm}_R.csv
# #             
# #         done
# #         echo ""
# #     done
# # done
# 
# #### OTHER THINGS I'VE DONE ####
# #
# # Average depth of coverage for each sample:
# # for file in `ls -1 /data1/per_mtdna/results/bamfiles/3553-CT-????.bam`; do echo -ne "${file} "; samtools-1.9 depth -d 5000000 -r ${mtchr}:5962-14750 ${file} | awk '{sum += $3; sumsq += ($3)^2} END {printf "%f %f \n", sum/NR, sqrt((sumsq-sum^2/NR)/NR)}'; done
# # Average depth in areas outside the main deletion
# # for n in `seq 91 99`; do 
# #     echo -ne "$n "
# #     start=$( cut -f 1,3,4,10 -d ',' all_deletions_1000_100.csv | sed 's/,/ /g' | grep "^${n}" | sort -n -k4 | tail -1 | cut -f2 -d' ' )
# #     end=$( cut -f 1,3,4,10 -d ',' all_deletions_1000_100.csv | sed 's/,//g' | grep "^${n}" | sort -n -k4 | tail -1 | cut -f3 -d' ' )
# #     ( samtools-1.9 depth -d 5000000 -r ${mtchr}:5962-${start} /data1/per_mtdna/results/bamfiles/3553-CT-00${n}.bam && samtools-1.9 depth -d 5000000 -r ${mtchr}:${end}-14750 /data1/per_mtdna/results/bamfiles/3553-CT-00${n}.bam ) | \
# #     awk '{sum += $3; sumsq += ($3)^2} END {printf "%f %f \n", sum/NR, sqrt((sumsq-sum^2/NR)/NR)}'
# # done
# 
# # 
